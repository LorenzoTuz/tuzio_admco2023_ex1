# TUZIO_AdmCO2023_Ex1

# 1  Objectifs
Je devais commenter au mieux le code essai5.py pour ce faire il fallait le comprendre et le tester.
# 2 Test
Pour comprendre ce code j'ai réalisé plusieur test:

- Dans un premier temps j'ai juste affiché le plateau et ce n'est qu'une liste de 9 éléments.
![scripttest](selfboard.png)

- Ensuite je n'ai pas eu besoin de faire les tests avec différents (autre que random) pour la comprehension du code mais j'ai quand même affiché une parti entre computer 
![scripttest](parie_computer.png)
- J'ai aussi joué une partie contre l'ordinateur pour finir sur une égalité 
- Pour finir j'ai juste afficher étape par étape comment se déroulait une partie entre 2 joueur random afin de comprendre comment était géré la fin de partie.
![scripttest](partie_detail.png)

# 3 Conclusion 
Ce code semble correcte il fonctionnne et gère une partie de morpion.
