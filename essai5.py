#!/usr/bin/env python
# coding: utf-8
# Simple TicTacToe / morpion game in Python 
"""
author : TUZIO Lorenzo

Commenter le code au mieux 
"""


from datetime import datetime

import random
import sys

class morpion_board:
    '''Création de la classe du plateau pour définir ce dernier
    ainsi que toute les méthode associées à cette classe'''
    def __init__(self):
        '''Définition de la taille du plateau ainsi que des conditions de victoires'''
        self.board=[i for i in range(0,9)] # Création du plateau de jeu carré 3x3 donc liste de 9 éléments.
        #print(self.board)
        self.player1, self.player2 = '',''

        # Corners, Center and Others, respectively
        # tuple qui contient respectivement les coins le centre et les autres cases
        self.moves=((1,7,3,9),(5,),(2,4,6,8)) 
        # Winner combinations
        # Combbinaison où il faut placer son token pour remporter la victoire 
        self.winners=((0,1,2),(3,4,5),(6,7,8),(0,3,6),(1,4,7),(2,5,8),(0,4,8),(2,4,6)) 
        # Table
        self.tab=range(1,10)

    def print_board(self):
        '''Méthode qui gère l'affichage du plateau de morpion'''
        x=1
        for i in self.board:
            end = ' | ' # ajout d'une séparation de colonne 

            if x%3 == 0:   # création des lignes si on est dans la troisième colonne.
                end = ' \n'
                if i != 1: end+='---------\n';
            char=' '
            #permet de décaler l'affichage si un charactère est olacé dans une case 
            if i in ('X','O'): char=i; 
            x+=1
            print(char,end=end)
            
    def select_char(self):
        '''Permet l'attribution des X ou des O aléatoirement'''
        chars=('X','O')
        if random.randint(0,1) == 0:
            return chars[::-1]
        return chars

    def can_move(self,player, move):
        '''Test si le jouer peut se déplacer/jouer son tour'''
        if move in self.tab and self.board[move-1] == move-1: 
            return True
        return False

    def can_win(self,player, move):
        '''Gère la victoire'''
        places=[]
        x=0
        # test si la case contient un élément correspondant à un joueur 
        for i in self.board: 
            if i == self.player1: places.append(x);
            x+=1
        win=True
        # si places contient un des tuple qui offre la victoire alors le joueur gagne 
        for tup in self.winners: 
            win=True
            for ix in tup:
                if self.board[ix] != player:
                    win=False
                    break
            if win == True:
                break
        return win

    def make_move(self, player, move, undo=False):
        '''Méthode pour savoir si le joueur se déplace ou gagne'''
        if self.can_move(player, move):
            self.board[move-1] = player
            win=self.can_win(player, move)
            if undo:
                self.board[move-1] = move-1
            return (True, win)
        return (False, False)

    def random1_move(self):
        '''Joueur qui joue de manière aléatoire 1'''
        move=-1
        random.seed(datetime.now())
        # If I can win, others don't matter.
        # Génère un entier aléatoire dans la taille du plateau
        # si le bot peut jouer il le place au nombre généré 
        for i in random.sample(range(1,10),9):
            if self.can_move(self.player1, i):
                move=i
                break

        return self.make_move(self.player1, move)

    def random2_move(self):
        '''Joueur qui joue de manière aléatoire 2'''
        move=-1
        random.seed(datetime.now())
        # If I can win, others don't matter.
        # Génère un entier aléatoire dans la taille du plateau
        # si le bot peut jouer il le place au nombre généré 
        for i in random.sample(range(1,10),9):
            if self.can_move(self.player2, i):
                move=i
                break

        return self.make_move(self.player2, move)

    def human1_move(self):
        '''Joueur Humain 1'''
        self.print_board()
        print('# Make your move ! [1-9] : ', end='')
        move = int(input()) # attente de la selectiond 'une case pour ce joueur 
        return self.make_move(self.player1,move)

    def human2_move(self):
        '''Joueur Humain 2'''
        self.print_board()
        print('# Make your move ! [1-9] : ', end='')
        move = int(input()) # attente de la selectiond 'une case pour ce joueur 
        return self.make_move(self.player2,move)


    def computer1_move(self):
        '''Bot qui joue selon une méthode fixe 1'''
        move=-1
        # If I can win, others don't matter.
        # Attente de la possibilité de jouer 
        for i in range(1,10):
            if self.make_move(self.player1, i, True)[1]:
                move=i
                break
        if move == -1:
            # If other player can win, block him.
            # empeche la victoire de l'autre joueur si il peut gagner au prochain tour
            for i in range(1,10):
                if self.make_move(self.player2, i, True)[1]:
                    move=i
                    break
        if move == -1:
            # Otherwise, try to take one of desired places.
            # Joue à une place aléatoire sur le plateau si cela est possible
            for tup in self.moves:
                l=list(tup)
                random.shuffle(l)
                for mv in l :
                    if move == -1 and self.can_move(self.player1, mv):
                        move=mv
                        break
        return self.make_move(self.player1, move)

    def computer2_move(self):
        '''Bot qui joue selon une méthode fixe 2'''
        move=-1
        # If I can win, others don't matter.
        # Attente de la possibilité de jouer 
        for i in range(1,10):
            if self.make_move(self.player2, i, True)[1]:
                move=i
                break
        if move == -1:
            # If player can win, block him.
            # empeche la victoire de l'autre joueur si il peut gagner au prochain tour
            for i in range(1,10):
                if self.make_move(self.player1, i, True)[1]:
                    move=i
                    break
        if move == -1:
            # Otherwise, try to take one of desired places.
            # Joue à une place aléatoire sur le plateau si cela est possible
            for tup in (self.moves):
                l=list(tup)
                random.shuffle(l)
                for mv in l :
                    if move == -1 and self.can_move(self.player2,mv):
                        move=mv
                        break
        return self.make_move(self.player2, move)



    def space_exist(self):
        '''Gère la fin de parti si le compte de token est égal à 9'''
        return self.board.count('X') + self.board.count('O') != 9
    

    def playerAvsplayerB(self,playerA_move,playerB_move):
        self.player1='X'
        self.player2 = 'O'
        print('Player  A is [%s] and Player B is [%s]' % (self.player1, self.player2))
        result='%%% Deuce ! %%%'
        computer_first = True
        if computer_first==True :
            # test tant que exit n'est pas validé si le joeur peut jouer sinon c'est qu'il a gagné
            while self.space_exist():
                #self.print_board()
                if playerA_move()[1]:
                    
                    result='*** Congratulations ! Player A wins ! ***'
                    break
                #self.print_board()
                if playerB_move()[1]:
                    result='*** Congratulations ! Player B wins ! ***'
                    break;
        self.print_board()
        print(result)

game=morpion_board()
#game.playerAvsplayerB(game.human1_move,game.human2_move)
game.playerAvsplayerB(game.random1_move,game.random2_move)
#game.playerAvsplayerB(game.computer1_move,game.random2_move)
#game.playerAvsplayerB(game.random1_move,game.computer2_move)
#game.playerAvsplayerB(game.computer1_move,game.computer2_move)
#game.playerAvsplayerB(game.human1_move,game.computer2_move)
#game.playerAvsplayerB(game.computer1_move,game.human2_move)




